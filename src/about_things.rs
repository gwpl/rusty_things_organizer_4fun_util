#![allow(unused_imports)]
#![allow(dead_code)]

use derive_new::new;

use crate::things_tree::{IntoAsciiTree, ThingsTree};
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::error::Error;
use std::io;
use std::vec::Vec;

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, new)]
#[serde(rename_all = "PascalCase")]
pub struct AboutThingRecord {
    pub code: String,
    pub short_name: String,
    pub short_description: String,
    pub long_name: String,
    pub notes: String,
    pub rdfurl: String,
}

// TODO: make Key a custom type (?)
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
pub struct AboutThingMemDB {
    db: BTreeMap<String, AboutThingRecord>, // key is code
}

impl AboutThingRecord {
    pub fn from_strs(a: &str, b: &str, c: &str, d: &str, e: &str, f: &str) -> Self {
        AboutThingRecord {
            code: a.to_string(),
            short_name: b.to_string(),
            short_description: c.to_string(),
            long_name: d.to_string(),
            notes: e.to_string(),
            rdfurl: f.to_string(),
        }
    }
}

fn load_from_csv_to_vec<R>(input: R) -> Result<Vec<AboutThingRecord>, Box<dyn Error>>
where
    R: io::Read,
{
    let mut v: Vec<_> = Vec::new();
    let mut rdr = csv::Reader::from_reader(input);
    for result in rdr.deserialize() {
        let record: AboutThingRecord = result?;
        v.push(record);
    }
    Ok(v)
}

fn save_to_csv_from_vec<'a, W, I>(output: &mut W, iterator: I) -> Result<(), Box<dyn Error>>
where
    W: io::Write,
    I: Iterator<Item = &'a AboutThingRecord>,
{
    let mut wtr = csv::Writer::from_writer(output);
    for record in iterator {
        wtr.serialize(record)?;
    }
    Ok(())
}

pub trait UpdatableDB {
    fn update(&mut self, about_thing: &AboutThingRecord);
}

pub trait SearchableDB {
    fn search_by_thing_code(&self, code: &str) -> Option<&AboutThingRecord>;
}

impl UpdatableDB for AboutThingMemDB {
    fn update(&mut self, about_thing: &AboutThingRecord) {
        self.db
            .insert(about_thing.code.clone(), about_thing.clone());
    }
}

impl SearchableDB for AboutThingMemDB {
    fn search_by_thing_code(&self, code: &str) -> Option<&AboutThingRecord> {
        self.db.get(code)
    }
}

impl AboutThingMemDB {
    pub fn new() -> AboutThingMemDB {
        AboutThingMemDB {
            db: BTreeMap::new(),
        }
    }

    pub fn len(&self) -> usize {
        self.db.len()
    }

    // Adds elements from csv reader (e.g. file).
    // If elements alredy exised, they will be updated with new values
    // and `last_updated` entry is ignored in decision making.
    pub fn add_from_csv<R>(&mut self, input: R) -> Result<(), Box<dyn Error>>
    where
        R: io::Read,
    {
        let mut rdr = csv::Reader::from_reader(input);
        for result in rdr.deserialize() {
            let record: AboutThingRecord = result?;
            self.update(&record);
        }
        Ok(())
    }

    // dumps as csv into writer
    pub fn into_csv_from_db<W>(&self, output: &mut W) -> Result<(), Box<dyn Error>>
    where
        W: io::Write,
    {
        let mut wrt = csv::Writer::from_writer(output);
        for (code, tprop) in &self.db {
            assert_eq!(*code, tprop.code);
            wrt.serialize(&tprop)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod whatwheretests {
    use super::*;
    use pretty_assertions::assert_eq;

    const CSV00IN: &str = "Code,ShortName,ShortDescription,LongName,Notes,Rdfurl
a,aaa,a desc,aaaaa,a notes,https://x/a
b,bbb,b desc,bbbbb,\"b, \"\"notes\"\"\",https://x/b
";

    fn records00() -> [AboutThingRecord; 2] {
        [
            AboutThingRecord::from_strs("a", "aaa", "a desc", "aaaaa", "a notes", "https://x/a"),
            AboutThingRecord::from_strs(
                "b",
                "bbb",
                "b desc",
                "bbbbb",
                "b, \"notes\"",
                "https://x/b",
            ),
        ]
    }

    #[test]
    fn test_load_from_csv_to_vec_00() -> Result<(), Box<dyn Error>> {
        let mut buff = io::Cursor::new(CSV00IN);
        let v = load_from_csv_to_vec(buff)?;
        assert_eq!(&v, &records00());
        Ok(())
    }

    #[test]
    fn test_save_to_csv_from_vec_00() -> Result<(), Box<dyn Error>> {
        let mut output_as_bytes: Vec<u8> = Vec::new();
        let records = records00();
        save_to_csv_from_vec(&mut output_as_bytes, records.iter())?;
        let output_string = String::from_utf8(output_as_bytes).expect("Not UTF-8");
        assert_eq!(output_string.trim(), CSV00IN.trim());
        Ok(())
    }

    #[test]
    fn test_add_from_csv_to_db_00() -> Result<(), Box<dyn Error>> {
        let mut db = AboutThingMemDB::new();
        let mut buff = io::Cursor::new(CSV00IN);
        db.add_from_csv(buff)?;
        let records = records00();
        assert_eq!(db.search_by_thing_code("a"), Some(&records[0]));
        assert_eq!(db.search_by_thing_code("b"), Some(&records[1]));
        assert_eq!(db.len(), 2);
        Ok(())
    }
}
